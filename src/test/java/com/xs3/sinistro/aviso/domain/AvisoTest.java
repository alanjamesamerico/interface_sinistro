package com.xs3.sinistro.aviso.domain;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xs3.sinistro.aviso.domain.dto.InformanteDTO;
import com.xs3.sinistro.aviso.domain.dto.OcorrenciaDTO;
import com.xs3.sinistro.aviso.domain.dto.SeguradoDTO;

class AvisoTest {

	private final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	@Test
	public void gerarListaAvisosJsonTest() {
		System.out.println(gson.toJson(this.gerarListaAviso()));
	}
	
	@Test
	public void gerarListaComUmAvisoJson() {
		System.out.println(gson.toJson(this.gerarUmAviso()));
	}
	
	
	private List<AvisoSinistro> gerarListaAviso() {
		
		SeguradoDTO segurado = SeguradoDTO.builder().nome("Fulanildo da Costa Neto").endereco("rua 3").build();
		InformanteDTO informante = InformanteDTO.builder().nome("Fulanildo da Costa Neto").endreco("rua 3").build();
		OcorrenciaDTO ocorrencia = OcorrenciaDTO.builder().descricao("Telhado da casa saiu voando.")
					  .valorEstimado(new BigDecimal(5000))
					  .build();
		AvisoSinistro aviso = AvisoSinistro.builder().segurado(segurado).informante(informante).ocorrencia(ocorrencia).build();


		SeguradoDTO segurado2 = SeguradoDTO.builder().nome("Beltranildo da Costa Neto").endereco("rua 3").build();
		InformanteDTO informante2 = InformanteDTO.builder().nome("Beltranildo da Costa Neto").endreco("rua 3").build();
		OcorrenciaDTO ocorrencia2 = OcorrenciaDTO.builder().descricao("Cano da casa estourou.")
					  .valorEstimado(new BigDecimal(2000))
					  .build();
		AvisoSinistro aviso2 = AvisoSinistro.builder().segurado(segurado2).informante(informante2).ocorrencia(ocorrencia2).build();
		
		
		SeguradoDTO segurado3 = SeguradoDTO.builder().nome("Ciclano da Costa Neto").endereco("rua 3").build();
		InformanteDTO informante3 = InformanteDTO.builder().nome("Ciclano da Costa Neto").endreco("rua 3").build();
		OcorrenciaDTO ocorrencia3 = OcorrenciaDTO.builder().descricao("Casa pegou fogo")
					  .valorEstimado(new BigDecimal(374000))
					  .build();
		AvisoSinistro aviso3 = AvisoSinistro.builder().segurado(segurado3).informante(informante3).ocorrencia(ocorrencia3).build();
		
		return Lists.newArrayList(aviso, aviso2, aviso3);
	}
	
	private AvisoSinistro gerarUmAviso() {
		
		SeguradoDTO segurado = SeguradoDTO.builder().nome("Cassandra dos Santos").endereco("rua 3").build();
		InformanteDTO informante = InformanteDTO.builder().nome("Cassandra dos Santos").endreco("rua 3").build();
		OcorrenciaDTO ocorrencia = OcorrenciaDTO.builder().descricao("A casa alagou e deu PT.")
					  .valorEstimado(new BigDecimal(269000))
					  .build();
		
		AvisoSinistro aviso = AvisoSinistro.builder().segurado(segurado).informante(informante).ocorrencia(ocorrencia).build();
		
		return aviso;
	}
}
