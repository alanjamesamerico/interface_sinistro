package com.xs3.sinistro.aviso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xs3.sinistro.aviso.domain.AvisoSinistro;
import com.xs3.sinistro.aviso.servce.AvisoService;

@RestController
@RequestMapping("/avisos")
public class AvisoController {
	
	@Autowired
	private AvisoService service;
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> buscarPorId(@PathVariable String id) {
		return service.buscarPorId(id);
	}
	
	
	@PostMapping
	@ResponseBody
	private ResponseEntity<?> salvarAviso(@RequestBody AvisoSinistro aviso) {
		return service.salvarAviso(aviso);
	}
	
	
	@PostMapping("/all")
	@ResponseBody
	private ResponseEntity<?> salvarListaAvisos(@RequestBody List<AvisoSinistro> avisos) {
		return service.salvarListaAvisos(avisos);
	}
}