package com.xs3.sinistro.aviso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvisoSinistroApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvisoSinistroApplication.class, args);
	}
}
