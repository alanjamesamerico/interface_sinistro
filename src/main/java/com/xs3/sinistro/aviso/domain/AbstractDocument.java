package com.xs3.sinistro.aviso.domain;

import org.springframework.data.annotation.Id;

import lombok.Getter;

@Getter
public abstract class AbstractDocument {
	
	@Id
	private String id;
}
