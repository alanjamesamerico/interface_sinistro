package com.xs3.sinistro.aviso.domain.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class OcorrenciaDTO {
	
	private String descricao;
	
	private BigDecimal valorEstimado;
	
//	private LocalDateTime dataOcorrencia;
}
