package com.xs3.sinistro.aviso.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
public class AvisoDTO {
	
	private SeguradoDTO segurado;
	
	private InformanteDTO informante;
	
	private OcorrenciaDTO ocorrencia;
}
