package com.xs3.sinistro.aviso.domain.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RespostaAvisoDTO {
	
	private int codRetorno;
	
	private int numAviso;
	
	private String dsRetorno;
	
	private String numSinistro;
}
