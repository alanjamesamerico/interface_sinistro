package com.xs3.sinistro.aviso.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class SeguradoDTO {
	
	private String nome;
	private String endereco;
}
