package com.xs3.sinistro.aviso.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.xs3.sinistro.aviso.domain.dto.InformanteDTO;
import com.xs3.sinistro.aviso.domain.dto.OcorrenciaDTO;
import com.xs3.sinistro.aviso.domain.dto.SeguradoDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "aviso")
public class AvisoSinistro extends AbstractDocument {
	
	private SeguradoDTO segurado;
	
	private InformanteDTO informante;
	
	private OcorrenciaDTO ocorrencia;
}
