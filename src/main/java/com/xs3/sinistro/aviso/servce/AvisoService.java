package com.xs3.sinistro.aviso.servce;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.xs3.sinistro.aviso.domain.AvisoSinistro;
import com.xs3.sinistro.aviso.domain.dto.RespostaAvisoDTO;
import com.xs3.sinistro.aviso.excepiton.AvisoNotFound;
import com.xs3.sinistro.aviso.excepiton.PersistenceException;
import com.xs3.sinistro.aviso.repository.AvisoRepository;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class AvisoService {
	
	@Autowired
	private AvisoRepository repository;
	
	
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public ResponseEntity<?> salvarAviso(AvisoSinistro aviso) {
		
		AvisoSinistro avisoSinistro = this.repository.save(aviso);
		
		if (avisoSinistro == null) {
			log.error("Não foi possível salvar aviso!", new PersistenceException(aviso));
			return ResponseEntity.status(HttpStatus.ACCEPTED)
								 .body(new PersistenceException(aviso));
		}
		
		return ResponseEntity.status(HttpStatus.CREATED)
							 .body(this.gerarResposta());
	}
	
	
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public ResponseEntity<?> salvarListaAvisos(List<AvisoSinistro> avisos) {
		
		List<AvisoSinistro> avisoRepository = this.repository.saveAll(avisos);
		
		if (avisoRepository == null) {
			log.error("Não foi possível salvar a lista de avisos!", new PersistenceException(avisos));
			return ResponseEntity.status(HttpStatus.ACCEPTED)
								 .body(new PersistenceException(avisos));
		}
		
		return ResponseEntity.status(HttpStatus.CREATED)
							 .body(this.gerarResposta());
	}

	
	public ResponseEntity<?> buscarPorId(String id) {
		
		Optional<AvisoSinistro> avisoRepository = this.repository.findById(id);
		
		if(!avisoRepository.isPresent()) {
			log.error("Não foi buscar aviso com id: {}", id, new AvisoNotFound(id));
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(avisoRepository.get());
	}
	
	
	public RespostaAvisoDTO gerarResposta() {
		return RespostaAvisoDTO.builder().codRetorno(0)
									  .dsRetorno("Sucesso, aviso de AvisoSinistro Gerado")
									  .numAviso(123123)
									  .numSinistro("J1065099999").build();
	}
}
