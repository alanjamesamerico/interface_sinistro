package com.xs3.sinistro.aviso.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xs3.sinistro.aviso.domain.AvisoSinistro;

public interface AvisoRepository extends MongoRepository <AvisoSinistro, String> {

}
