package com.xs3.sinistro.aviso.excepiton;

public class AvisoNotFound extends RuntimeException {
	
	private static final long serialVersionUID = 4523387944009482131L;

	public AvisoNotFound(String id) {
		super("AvisoSinistro não encontrado. ID do aviso: " + id);
	}
}
