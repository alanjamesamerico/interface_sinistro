package com.xs3.sinistro.aviso.excepiton;

public class PersistenceException extends RuntimeException {

	private static final long serialVersionUID = 7888657720130886412L;
	
	public PersistenceException(Object object) {
		super("Não foi possível salvar o aviso: " + object.toString());
	}
}
